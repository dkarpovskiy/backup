#!/bin/bash

# Название volume mariadb
MARIADB_VOLUME=mariadb
# Название volume nextcloud хранилища
NEXTCLOUD_VOLUME=docker-onlyoffice-nextcloud_app_data
# Image для создания backup'ов
IMAGE_FOR_BACKUP=debian
# Локальная директория для хранения и обработки бэкапов
BACKUP_DIR=/backup
