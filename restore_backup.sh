#!/bin/bash

source variables.sh


rm -rf $BACKUP_DIR/tmp/*

copy_backup() {
    if [ $2 -eq 0 ]; then
        if [ "$(ls -A $BACKUP_DIR/data/$1/3)" ]; then
            cp $BACKUP_DIR/data/$1/3/*  $BACKUP_DIR/tmp/
        elif [ "$(ls -A $BACKUP_DIR/data/$1/2)" ]; then
            cp $BACKUP_DIR/data/$1/2/*  $BACKUP_DIR/tmp/
        else [ "$(ls -A $BACKUP_DIR/data/$1/1)" ]
            cp $BACKUP_DIR/data/$1/1/*  $BACKUP_DIR/tmp/
        fi
    else
        if [ ! -z "$(find $BACKUP_DIR/data/$1/3 -mtime $2)" ]; then
            cp $(find $BACKUP_DIR/data/$1/3 -mtime +$(($2 - 1))) $BACKUP_DIR/tmp/
        elif [ ! -z "$(find $BACKUP_DIR/data/$1/2 -mtime $2)" ]; then
            cp $(find $BACKUP_DIR/data/$1/2 -mtime +$(($2 - 1))) $BACKUP_DIR/tmp/
        elif [ ! -z "$(find $BACKUP_DIR/data/$1/1 -mtime $2)" ]; then
            cp $(find $BACKUP_DIR/data/$1/1 -mtime +$(($2 - 1))) $BACKUP_DIR/tmp/
        else
            echo "Бэкап не найден $1. Выход"
            exit 1
        fi
    fi
}

restore_backup() {
    echo "Восстановление бэкапа $1"

    docker run --rm -it -v $BACKUP_DIR/tmp/:/backup-tar -v $1:/data $IMAGE_FOR_BACKUP /bin/bash -c "for f in /backup-tar/*.tgz; do tar xf \"\$f\"; done; rm -rf /data/*; mv /backup-data/* /data/"

}


if [ -z "$1" ]; then
    echo "Аргумент с откатом не задан, использвется последний возможный бэкап"
    ROLLBACK=0
else
    echo "Откат на $1 дней"
    ROLLBACK=$1
fi

copy_backup nextcloud $ROLLBACK
restore_backup $NEXTCLOUD_VOLUME

rm -rf $BACKUP_DIR/tmp/*

copy_backup database $ROLLBACK
restore_backup $MARIADB_VOLUME
