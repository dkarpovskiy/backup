#!/bin/bash

# Пользователь и хост для подключения через ssh/rsync
HOST=remote_user@remote_host
# Удаленная директория на сервере, из которой берутся изменения (главная директория)
REMOTE_BACKUP_DIR=/backup/data/
# Локальная директория на сервере, в которую кладутся изменения (зависимая директория)
LOCAL_BACKUP_DIR=/backup/

rsync -rvh $HOST:$REMOTE_BACKUP_DIR $LOCAL_BACKUP_DIR --delete --no-perms -O

