#!/bin/bash

source variables.sh


mkdir -p $BACKUP_DIR/tmp $BACKUP_DIR/data $BACKUP_DIR/incremental
rm -rf $BACKUP_DIR/tmp/*


create_backup() {
    mkdir -p $BACKUP_DIR/data/$1/1 $BACKUP_DIR/data/$1/2 $BACKUP_DIR/data/$1/3 $BACKUP_DIR/incremental/$1
    if [ $(ls $BACKUP_DIR/data/$1/1 | wc -l) -lt 7 ]; then
        CURRENT_BACKUP_DIR=$BACKUP_DIR/data/$1/1
    elif [ $(ls $BACKUP_DIR/data/$1/2 | wc -l) -lt 7 ]; then
        CURRENT_BACKUP_DIR=$BACKUP_DIR/data/$1/2
    elif [ $(ls $BACKUP_DIR/data/$1/3 | wc -l) -lt 7 ]; then
        CURRENT_BACKUP_DIR=$BACKUP_DIR/data/$1/3
    else
        echo "Удаление утсаревших бэкапов $1"
        rm -rf $BACKUP_DIR/data/$1/1
        mv $BACKUP_DIR/data/$1/2 $BACKUP_DIR/data/$1/1
        mv $BACKUP_DIR/data/$1/3 $BACKUP_DIR/data/$1/2
        mkdir -p $BACKUP_DIR/data/$1/3
        CURRENT_BACKUP_DIR=$BACKUP_DIR/data/$1/3
    fi

    if [ $(ls $CURRENT_BACKUP_DIR | wc -l) -eq 0 ]; then
        echo "Создание полного бэкапа $1"
        rm -f $BACKUP_DIR/incremental/$1/*
    else
        echo "Создание инкрементального бэкапа $1"
    fi

    docker run --rm -v $2:/backup-data -v $BACKUP_DIR/tmp:/backup-result -v $BACKUP_DIR/incremental/$1:/backup-incremental $IMAGE_FOR_BACKUP tar --absolute-names --create --gzip --file=/backup-result/backup.tgz --listed-incremental=/backup-incremental/incremental.snar /backup-data/ 

    mv $BACKUP_DIR/tmp/backup.tgz $CURRENT_BACKUP_DIR/backup-$(date +'%Y_%m_%d_%H_%M_%S').tgz

    echo "Бэкап создан - $CURRENT_BACKUP_DIR/backup-$(date +'%Y_%m_%d_%H_%M_%S').tgz"

}

create_backup database $MARIADB_VOLUME
create_backup nextcloud $NEXTCLOUD_VOLUME
